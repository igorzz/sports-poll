import React from "react";

import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

import Poll from "./Poll";
import Logo from "./Logo/Logo";

configure({ adapter: new Adapter() });

const setUp = (props = {}) => {
  const component = shallow(<Poll {...props} />);
  return component;
};

describe("<Poll/>", () => {
  let wrapper;

  beforeEach(() => {
    const props = {
      poll: {
        sport: "some sport",
        awayName: "some name",
        homeName: "some name",
        state: "some state"
      }
    };
    wrapper = setUp(props);
  });

  it("Should render 2 logo components", () => {
    expect(wrapper.find(Logo)).toHaveLength(2);
  });
});
