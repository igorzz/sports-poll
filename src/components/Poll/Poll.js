import React from "react";
import classes from "./Poll.css";
import Header from "./Header/Header";
import Logo from "./Logo/Logo";
import PollingOptions from "./PollingOptions/PollingOptions";

const poll = props => {
  return (
    <div className={classes.Poll}>
      <Header sport={props.poll.sport} country={props.poll.country} />
      <div className={classes.Body}>
        <div className={classes.LogoWrapper}>
          <Logo image={props.poll.homeImage} />
          <Logo image={props.poll.awayImage} />
        </div>
        <h3>{props.poll.name}</h3>
        <p>Group: {props.poll.group}</p>
        <p>State: {props.poll.state.replace("_", " ").toLowerCase()}</p>
        <PollingOptions
          homeName={props.poll.homeName}
          awayName={props.poll.awayName}
          sport={props.poll.sport}
          id={props.poll.id}
          pollingOptionSelected={props.pollOptionSelected}
        />
      </div>
    </div>
  );
};

export default poll;
