import React from "react";
import classes from "./Header.css";
import football from "../../../assets/images/football.svg";
import snooker from "../../../assets/images/snooker.svg";
import handball from "../../../assets/images/handball.svg";
import tennis from "../../../assets/images/tennis.svg";
import ice_hockey from "../../../assets/images/ice_hockey.svg";

const header = props => {
  let icon = null;
  switch (props.sport.toLowerCase()) {
    case "football":
      icon = <img src={football} alt="" />;
      break;
    case "snooker":
      icon = <img src={snooker} alt="" />;
      break;
    case "handball":
      icon = <img src={handball} alt="" />;
      break;
    case "tennis":
      icon = <img src={tennis} alt="" />;
      break;
    case "ice_hockey":
      icon = <img src={ice_hockey} alt="" />;
      break;
    default:
      break;
  }
  return (
    <div className={classes.Header}>
      <div className={classes.HeaderLeft}>
        {icon}
        <p>
          <strong>Sport:</strong> {props.sport.toLowerCase()}
        </p>
      </div>
      <div className={classes.HeaderRight}>
        <h3>
          <strong>{props.sport.toLowerCase()}</strong> Poll
        </h3>
        <p>Country: {props.country.toLowerCase()}</p>
      </div>
    </div>
  );
};

export default header;
