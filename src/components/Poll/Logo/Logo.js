import React from "react";
import classes from "./Logo.css";

const logo = props => {
  if (!props.image) {
    return null;
  }

  return (
    <div className={classes.Logo} data-test="Logo">
      <img src={props.image} alt="" data-test="ImgLogo" />
    </div>
  );
};

export default logo;
