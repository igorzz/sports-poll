import React from "react";

import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

import Logo from "./Logo";

configure({ adapter: new Adapter() });

const setUp = (props = {}) => {
  const component = shallow(<Logo {...props} />);
  return component;
};

const findByAttribute = (component, attr) => {
  const wrapper = component.find(`[data-test='${attr}']`);
  return wrapper;
};

describe("<Logo/>", () => {
  describe("Have props", () => {
    let wrapper;

    beforeEach(() => {
      const props = {
        image: "some image"
      };
      wrapper = setUp(props);
    });

    it("Should render without errors", () => {
      expect(findByAttribute(wrapper, "Logo")).toHaveLength(1);
    });

    it("Should render a <img/>", () => {
      expect(findByAttribute(wrapper, "ImgLogo")).toHaveLength(1);
    });
  });

  describe("Have NO props", () => {
    let wrapper;

    beforeEach(() => {
      wrapper = setUp();
    });

    it("Should not render", () => {
      expect(findByAttribute(wrapper, "Logo")).toHaveLength(0);
    });
  });
});
