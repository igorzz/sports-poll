import React from "react";

import { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

import PollingOptions from "./PollingOptions";
import Button from "../../UI/Button/Button";

configure({ adapter: new Adapter() });
let wrapper;

beforeEach(() => {
  wrapper = shallow(<PollingOptions />);
});

describe("<PollingOptions/>", () => {
  it("Should render without errors", () => {
    expect(wrapper.find(`[data-test='PollingOptions']`)).toHaveLength(1);
  });

  it("Should render 3 button components", () => {
    expect(wrapper.find(Button)).toHaveLength(3);
  });
});
