import React from "react";
import classes from "./PollingOptions.css";
import Button from "../../UI/Button/Button";

const pollingOptions = props => {
  return (
    <div className={classes.PollingOptions} data-test="PollingOptions">
      <Button
        btnType="Win"
        clicked={() =>
          props.pollingOptionSelected({
            option: props.homeName,
            sport: props.sport,
            id: props.id
          })
        }
      >
        {props.homeName} win
      </Button>
      <Button
        btnType="Draw"
        clicked={() =>
          props.pollingOptionSelected({
            option: "Draw",
            sport: props.sport,
            id: props.id
          })
        }
      >
        Draw
      </Button>
      <Button
        btnType="Win"
        clicked={() =>
          props.pollingOptionSelected({
            option: props.awayName,
            sport: props.sport,
            id: props.id
          })
        }
      >
        {props.awayName} win
      </Button>
    </div>
  );
};

export default pollingOptions;
