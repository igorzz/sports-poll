import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://sports-poll-6f292.firebaseio.com/'
})

export default instance;