import React, { Component } from "react";
import classes from "./App.css";
import Poll from "./components/Poll/Poll";
import axios from "./axios-events";
import Spinner from "./components/UI/Spinner/Spinner";

class App extends Component {
  state = {
    polls: null,
    randomPoll: null
  };

  componentDidMount() {
    axios
      .get("/polls.json")
      .then(response => {
        this.setState({ polls: response.data });
        this.selectRandomPollHandler(null);
      })
      .catch(error => console.log(error));
  }

  selectRandomPollHandler = lastVote => {
    let newRandomPoll = null;
    if (lastVote) {
      const votes = this.getLocalStorageHandler();
      if (votes.length > 18) {
        localStorage.clear();
      }
      const allCurrentSportEventIds = this.state.polls
        .filter(poll => {
          return poll.sport === lastVote.sport;
        })
        .map(vote => {
          return vote.id;
        });
      const allSportVoteIds = votes
        .filter(vote => {
          return vote.sport === lastVote.sport;
        })
        .map(vote => {
          return vote.id;
        });
      let eventIDs = allCurrentSportEventIds.filter(e => {
        return allSportVoteIds.indexOf(e) === -1;
      });

      if (eventIDs.length !== 0) {
        let randomID = this.state.polls !== null ? this.rand(eventIDs) : null;
        newRandomPoll = this.state.polls
          .filter(poll => {
            return poll.id === randomID;
          })
          .pop();
      } else {
        newRandomPoll =
          this.state.polls !== null ? this.rand(this.state.polls) : null;
      }
    } else {
      newRandomPoll =
        this.state.polls !== null ? this.rand(this.state.polls) : null;
    }
    this.setState({
      randomPoll: newRandomPoll
    });
  };

  getLocalStorageHandler = () => {
    const votes = JSON.parse(localStorage.getItem("votes"));
    return votes;
  };

  pollOptionSelectedHandler = poll => {
    // localStorage.clear();
    const votes = this.getLocalStorageHandler();
    let updatedVotes = votes ? [...votes] : [];
    updatedVotes.push({
      option: poll.option,
      sport: poll.sport,
      id: poll.id
    });
    window.localStorage.setItem("votes", JSON.stringify(updatedVotes));
    this.selectRandomPollHandler(poll);
  };

  rand = list => {
    return list[Math.floor(Math.random() * list.length)];
  };

  render() {
    let poll =
      this.state.randomPoll === null ? (
        <Spinner />
      ) : (
        <Poll
          poll={this.state.randomPoll}
          pollOptionSelected={this.pollOptionSelectedHandler}
        />
      );

    return <div className={classes.App}>{poll}</div>;
  }
}

export default App;
